# MERN stack

Building A React CRUD Application: ToDoList.

![](client/public/todo.png)

## Tools

- React
- NodeJS
- Express
- MongoDB

## Features

- Create todo
- Update todo
- Delete todo
- Search todo
- Pagination

## Installation

#### Backend (build container)

```sh
cd server
make build
```

#### Frontend (build container)

```sh
cd client
make build
```

#### Run all container

```sh
make run
```

| App url                | Api url                   |
| ---------------------- | ------------------------- |
| http://localhost:3000/ | http://localhost:5000/api |
