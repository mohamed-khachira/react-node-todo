import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import { Table, TodoStatus } from "../components";
import api from "../api";

const Styles = styled.div`
  table {
    thead {
      background: #2980b9;
      color: #ffffff;
      font-weight: 900;
    }
    width: 100%;
    border-spacing: 0;
    border: 1px solid black;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }
    th,
    td {
      margin: 0;
      padding: 1rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      :last-child {
        border-right: 0;
      }
    }
  }
`;

const Delete = styled.button.attrs({
  className: "btn btn-sm btn-danger m-1",
})`
  cursor: pointer; ;
`;

const Complete = styled.button.attrs({
  className: "btn btn-sm btn-success m-1",
})`
  cursor: pointer; ;
`;

/*const Edit = styled.button.attrs({
  className: "btn btn-sm btn-info m-1",
})`
  cursor: pointer; ;
`;*/

const SpanComplete = styled.span.attrs({})`
  text-decoration: line-through;
`;

function TodosList() {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(true);
  const columns = [
    {
      Header: "ID",
      accessor: "_id",
    },
    {
      Header: "Name",
      accessor: "name",
      Cell: (props) => {
        //return <span>{props.cell.value}HH</span>;
        if (props.row.original.status) {
          return <SpanComplete>{props.cell.value}</SpanComplete>;
        }
        return <span>{props.cell.value}</span>;
      },
    },
    {
      Header: "Description",
      accessor: "description",
      Cell: (props) => {
        //return <span>{props.cell.value}HH</span>;
        if (props.row.original.status) {
          return <SpanComplete>{props.cell.value}</SpanComplete>;
        }
        return <span>{props.cell.value}</span>;
      },
    },
    {
      Header: "Status",
      accessor: "status",
      filterable: true,
      // Cell method will provide the cell value; we pass it to render a custom component
      Cell: ({ cell: { value } }) => <TodoStatus value={value} />,
    },
    {
      Header: "Created date",
      accessor: "createdAt",
      Cell: ({ cell: { value } }) => (
        <span>
          {new Date(value).toLocaleDateString("en-US")}{" "}
          {new Date(value).toLocaleTimeString("en-US")}
        </span>
      ),
    },
    {
      Header: "Updated date",
      accessor: "updatedAt",
      Cell: ({ cell: { value } }) => (
        <span>
          {new Date(value).toLocaleDateString("en-US")}{" "}
          {new Date(value).toLocaleTimeString("en-US")}
        </span>
      ),
    },
    {
      Header: "Action",
      accessor: "action",
      width: "25%",
      Cell: ({ row: { original } }) => (
        <div>
          <Delete onClick={() => handleDelete(original._id)}>Delete</Delete>
          {!original.status && (
            <>
              <Complete onClick={() => handleComplete(original._id)}>
                Complete
              </Complete>
              <Link
                className="btn btn-sm btn-info m-1"
                to={"/todos/update/" + original._id}
              >
                Edit
              </Link>
            </>
          )}
        </div>
      ),
    },
  ];
  const fetchTodos = async () => {
    try {
      const data = await api.getAllTodos();
      setTodos(data.data.data);
      setLoading(false);
    } catch (error) {}
  };
  const handleDelete = async (id) => {
    try {
      api.deleteTodoById(id).then((result) => {
        fetchTodos();
      });
    } catch (error) {}
  };

  const handleComplete = (id) => {
    try {
      api.updateTodoById(id, { status: true }).then((result) => {
        fetchTodos();
      });
    } catch (error) {}
  };

  useEffect(() => {
    fetchTodos();
  }, []);

  return (
    <Styles>{!loading && <Table columns={columns} data={todos} />}</Styles>
  );
}

export default TodosList;
