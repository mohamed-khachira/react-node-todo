import React, { useState } from "react";
import api from "../api";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Title = styled.h1.attrs({
  className: "h1 text-center",
})``;

const Wrapper = styled.div.attrs({
  className: "form-group",
})`
  margin: 0 30px;
`;

const InputText = styled.input.attrs({
  className: "form-control",
})`
  margin: 5px;
`;

const Label = styled.label`
  margin: 5px;
`;
const Button = styled.button.attrs({
  className: `btn btn-primary`,
  type: `submit`,
})`
  margin: 15px 15px 15px 5px;
`;

/*const CancelButton = styled.a.attrs({
  className: `btn btn-danger`,
})`
  margin: 15px 15px 15px 5px;
`;*/

function TodoInsert() {
  const [todo, setTodo] = useState({
    name: "",
    description: "",
    status: false,
  });
  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    setTodo({ ...todo, [name]: value });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    await api.insertTodo(todo).then((res) => {
      setTodo({ name: "", description: "", status: false });
    });
  };
  return (
    <Wrapper>
      <Title>Create Todo</Title>
      <form onSubmit={handleSubmit}>
        <Label>Name: </Label>
        <InputText
          type="text"
          name="name"
          value={todo.name}
          onChange={handleChange}
        />
        <Label>Description: </Label>
        <InputText
          type="text"
          name="description"
          value={todo.description}
          onChange={handleChange}
        />
        <Button>Add Todo</Button>
        {/*<CancelButton href={"/todos/list"}>Cancel</CancelButton>*/}
        <Link to="/todos/list" className="btn btn-danger">
          Cancel
        </Link>
      </form>
    </Wrapper>
  );
}

export default TodoInsert;
