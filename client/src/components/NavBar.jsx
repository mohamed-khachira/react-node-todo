import React from "react";
import styled from "styled-components";

import Links from "./Links";

const Nav = styled.nav.attrs({
  className: "navbar navbar-expand-lg navbar-dark bg-dark",
})`
  margin-bottom: 20 px;
`;

function NavBar() {
  return (
    <Nav>
      <Links />
    </Nav>
  );
}

export default NavBar;
