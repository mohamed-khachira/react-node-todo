import React from "react";

function GlobalFilter({ filter, setFilter }) {
  return (
    <span>
      Search:
      <input
        className="m-2"
        value={filter || ""}
        onChange={(e) => setFilter(e.target.value)}
      />
    </span>
  );
}

export default GlobalFilter;
