import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Collapse = styled.div.attrs({
  className: "collpase navbar-collapse",
})``;

const List = styled.div.attrs({
  className: "navbar-nav mr-auto",
})``;

const Item = styled.div.attrs({
  className: "collpase navbar-collapse",
})``;

function Links() {
  return (
    <React.Fragment>
      <span className="navbar-brand">Simple MERN Application</span>
      <Collapse>
        <List>
          <Item>
            <Link to="/todos/list" className="nav-link">
              List Todos
            </Link>
          </Item>
          <Item>
            <Link to="/todos/create" className="nav-link">
              Create Todo
            </Link>
          </Item>
        </List>
      </Collapse>
    </React.Fragment>
  );
}

export default Links;
