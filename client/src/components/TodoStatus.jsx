import React from "react";

function TodoStatus({ value }) {
  return (
    <span className={`p-2 badge ${value ? "badge-success" : "badge-warning"}`}>
      {value ? "Completed" : "Pending"}
    </span>
  );
}

export default TodoStatus;
