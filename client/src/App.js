import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import styled from "styled-components";
import { NavBar } from "./components";
import "bootstrap/dist/css/bootstrap.min.css";
import { TodosList, TodoInsert, TodoUpdate } from "./pages";

const Container = styled.div.attrs({
  className: "container my-5",
})``;

function App() {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Container>
          <Route exact path="/">
            <Redirect to="/todos/list" />
          </Route>
          <Route path="/todos/list" exact component={TodosList} />
          <Route path="/todos/create" exact component={TodoInsert} />
          <Route path="/todos/update/:id" exact component={TodoUpdate} />
        </Container>
      </Switch>
    </Router>
  );
}

export default App;
